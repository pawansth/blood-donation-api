<?php

namespace app\controllers;

use yii;
use app\models\Authentication;
use yii\web\Response;

class CheckTokenController extends \yii\web\Controller
{
  public $enableCsrfValidation = false;
  // public $config['csrf_protection'] = false;
    public function actionCheck()
    {
      // $value;
      // if(Yii::$app->request->isPost) {
      //   $value = Yii::$app->request->post('accessToken');
      //   $value = json_encode($value);
      // } else {
      //   $value = 0;
      // }
      // echo  $value;
      $request = Yii::$app->request;
      // Yii::error(print_r($request));
      $accessToken = $request->post('accessToken');

      // Yii::$app->response->format = Response::FORMAT_JSON;
      // return ['accessToken'=>$accessToken];
      $model = new Authentication();

      $result = Authentication::find()
    ->where(['accessToken' => trim($accessToken)])
    ->one();

    Yii::$app->response->format = Response::FORMAT_JSON;
    if(!empty($result)) {
      return ['success' => true];
    } else {
      return ['success' => false];
    }

    }

}
