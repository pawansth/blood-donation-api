<?php

namespace app\controllers;

use Yii;
use app\models\SurveyRecord;
use app\models\SurveyRecordSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;

use app\controllers\UploadedFile;

class SurveyController extends Controller {



    public function actionSurveycreate() {

        if (isset($_POST)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $hidden = $_POST['accessToken'];
            $model = \app\models\Authentication::findOne(['accessToken'=>$hidden]);
            if (!empty($model)) {
                $surveyor = $model->id;
                $name = $_POST['personName'];
                $address = $_POST['address'];
                $mobileNo = $_POST['mobile'];
                $jobProfile = $_POST['jobProfile'];
                $natureBusiness = $_POST['natureBusiness'];
                $officeAddress = $_POST['officeAddress'];
                $officeContact = $_POST['officeContact'];
                $birthDate = $_POST['birthDate'];
                $opinon = $_POST['opinon'];
                $buyingNeeds = $_POST['buyingNeeds'];
                $imagePath = '';
                $image = $_POST['image'];
                if(!empty($image)) {
                    $imagePath = Yii::getAlias('@web')."images/".$name.'-'.$mobileNo.".jpg";
                    $uploadedFileObj = new UploadedFile();
                    if($uploadedFileObj->saveAs($image, $imagePath)) {} else {
                        return ["title"=>"Failed","message"=>"Error occur while uploading image"];
                    }
                }
                // $file= $_FILES['file']['name'];
                // $image = $name.'-'.$mobileNo;
                // $file_extension = explode('.',$file);
                // $destFile = Yii::getAlias('@web')."/images/".$image.".".$file_extension ;
                // move_uploaded_file($_FILES['file']['tmp_name'], $destFile);
                try{
                $insertquery = Yii::$app->db->createCommand()->insert('surveyRecord', [
                    "name" => $name,
                    "address" => $address,
                    "mobileNo" => $mobileNo,
                    "jobProfile" => $jobProfile,
                    "natureBusiness" => $natureBusiness,
                    "officeAddress" => $officeAddress,
                    "officeContact" => $officeContact,
                    "birthDate" => $birthDate,
                    "opinion" => $opinon,
                    "buyingNeeds" => $buyingNeeds,
                    "image"=>$imagePath,
                    "surveyor"=> $surveyor,
                    'createdDate'=>date('Y-m-d')
                ])->execute();
                }catch(\Exception $ex ){
                    return ["title"=>"error","message"=>$ex];
                }
                    if($insertquery){
                        return ["title"=>"Success","message"=>"data has been successfully inserted"];
                    }
            }
            else{
                 return ["title"=>"Failed","message"=>"Unable to submit data"];
            }
        }
    }

}
