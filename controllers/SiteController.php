<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
            if (!Yii::$app->user->isGuest)
            {

          $model=new \app\models\SurveyRecord();
         if ($model->load(Yii::$app->request->post())) {
            $model->surveyor= yii::$app->user->identity->id;
            $model->createdDate=date('Y-m-d');
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
            
            if ($model->file && $model->validate()) {
                $imagetoload =$model->name.'-'.$model->mobileNo;
                if( $model->file->saveAs('images/' . $imagetoload . '.' . $model->file->extension)) 
                {
                   $model->image = 'images/' . $imagetoload . '.' . $model->file->extension; 
                   $model->save();
                
                    Yii::$app->session->setFlash('success','You have successfully inserted the data');  
                   return $this->redirect(['site/index']);
               }


           }
           else {
            if ($model->validate()) {

                $model->save();
                 Yii::$app->session->setFlash('success','You have successfully inserted the data');  
                return $this->redirect(['site/index']);
            }
        
        }
      } 
     
        else{
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}

        else {
            $model = new LoginForm();
            return $this->redirect(['login']);
        }
   

    }



    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionSurveylist()
    {
         if(yii::$app->user->identity->userName=='mayank')
         {
           $searchModel = new \app\models\SurveyRecordSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('surveylist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    else
    {
         throw new \yii\web\NotFoundHttpException("You are not authorized to access this page");
        
    }
  }

}
