<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authentication".
 *
 * @property integer $id
 * @property string $userName
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property integer $status
 */
class Authentication extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface

{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authentication';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userName', 'password'], 'required'],
            [['status'], 'integer'],
            [['userName', 'password', 'authKey', 'accessToken'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userName' => 'User Name',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'status' => 'Status',
        ];
    }

       public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
      
    return $this->password ===md5($password);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    public static function findIdentityByAccessToken($token,$type=null)
    {
        return $this->accessToken;
    }
    public static function findbyUsername($uname)
    {
        return self::findOne(['userName'=>$uname]);
    }


    public function login($userName, $password) {
      $user = Authentication::find()
                    ->where([
                      'userName'=>$userName,
                      'password'=>$password
                    ])
                    ->one();
      
      if (!empty($user)) {
        $accessToken = $user['accessToken'];
        if (!empty(trim($accessToken))) {
          return $accessToken;
        } else {
          $date = new \DateTime();
          $addition = $date->getTimestamp();
          // generate new accessToken
          $randomString = Yii::$app->getSecurity()->generateRandomString(32);
          $finalAccessToken = $randomString.$addition;
          // update accessToken value to database table for corresponding table
          $result = \Yii::$app->db->createCommand("UPDATE authentication SET accessToken=:accessToken WHERE id=:id")
          ->bindValue(':accessToken', $finalAccessToken)
          ->bindValue(':id', $user['id'])
          ->execute();
          //  if statement that check value is successfully updated or not
          if ($result == 1) {
            return $finalAccessToken;
          } else {
            return false;
          }

        }
      } else {
        return false; // return false boolen value when user name and password is not matched
      }

    }

        public function getSurvey()
    {
        return $this->hasMany(SurveyRecord::className(), ['surveyor' => 'id']);
    }
}
